import { FormControl } from '@angular/forms';
import { expect } from 'chai';
import { spy } from 'sinon';
import { before } from '../../src/asserts';
import { validate } from '../../src/validate';
import { ValidationError } from '../../src/validation.error';

describe('Validators', () => {

  describe('Before', () => {

    let control: FormControl;

    beforeEach(() => {
      control = new FormControl(null);
    });

    it('Invalid', () => {
      control.setValue('2012-12-12');
      const assert = spy();
      before.apply({ _obj: control, assert }, ['2010-12-12']);
      expect(assert.calledOnce).to.be.true;
      expect(assert.args[0][0]).to.be.false;
    });

    it('Valid', () => {
      control.setValue('2010-12-12');
      const assert = spy();
      before.apply({ _obj: control, assert }, ['2012-12-12']);
      expect(assert.calledOnce).to.be.true;
      expect(assert.args[0][0]).to.be.true;
    });

    it('is.before invalid', () => {
      control.setValue('2030-12-12');
      try {
        validate(control).is.before(new Date(Date.now()));
        expect.fail('not throw', 'to throw');
      } catch (e) {
        expect(e).instanceOf(ValidationError);
      }
    });

    it('is.before valid', () => {
      control.setValue('2012-12-12');
      try {
        validate(control).is.before(new Date(Date.now()));
      } catch (e) {
        expect.fail('throwed', 'not to throw', e);
      }
    });

  });

});
