import { FormControl } from '@angular/forms';
import { expect } from 'chai';
import { spy } from 'sinon';
import { assertDate } from '../../src/asserts';
import { validate } from '../../src/validate';
import { ValidationError } from '../../src/validation.error';

describe('Validators', () => {

  describe('Date', () => {

    let control: FormControl;

    beforeEach(() => {
      control = new FormControl(null);
    });

    it('Invalid date formats', () => {
      const formatInvalidDates = [
        'invalid',
        '201',
        '2',
        '2010',
        '2010-',
        '2010-05',
        '2010-0',
        '2010-05-1',
        '2021-05-111',
        '201-255-10',
      ];

      for (const dateString of formatInvalidDates) {
        control.setValue(dateString);
        const assert = spy();
        assertDate.apply({ _obj: control, assert });
        expect(assert.calledOnce).to.be.true;
        expect(assert.args[0][0], `'${dateString}' is not a valid date`).to.be.false;
      }

    });

    it('Invalid dates', () => {
      const invalidDates = [
        '2010-13-01',
        '2018-01-35',
      ];

      for (const dateString of invalidDates) {
        control.setValue(dateString);
        const assert = spy();
        assertDate.apply({ _obj: control, assert });
        expect(assert.calledOnce).to.be.true;
        expect(assert.args[0][0], `'${dateString}' is not a valid date`).to.be.false;
      }
    });

    it('Invalid leap year', () => {
      control.setValue('2011-02-29');
      const assert = spy();
      assertDate.apply({ _obj: control, assert });
      expect(assert.calledOnce).to.be.true;
      expect(assert.args[0][0]).to.be.false;
    });

    it('Valid leap year', () => {
      control.setValue('2012-02-29');
      const assert = spy();
      assertDate.apply({ _obj: control, assert });
      expect(assert.calledOnce).to.be.true;
      expect(assert.args[0][0]).to.be.true;
    });

    it('Valid', () => {
      control.setValue('2010-12-12');
      const assert = spy();
      assertDate.apply({ _obj: control, assert });
      expect(assert.calledOnce).to.be.true;
      expect(assert.args[0][0]).to.be.true;
    });

    it('is.date invalid', () => {
      control.setValue('invalid');
      try {
        validate(control).is.date;
        expect.fail('not throw', 'to throw');
      } catch (e) {
        expect(e).instanceOf(ValidationError);
      }
    });

    it('is.date valid', () => {
      control.setValue('2012-12-12');
      try {
        validate(control).is.date;
      } catch (e) {
        expect.fail('throwed', 'not to throw', e);
      }
    });

  });

});
