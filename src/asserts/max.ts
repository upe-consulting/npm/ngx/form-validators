import { FormControl } from '@angular/forms';
import { Assertion } from '../assertion';
import { flag } from '../utils/flag';

export function assertMax(this: Assertion, max: number) {
  const control: FormControl = flag(this, 'object');

  if (control.value) {
    if (flag(this, 'number')) {
      this.assert(Number(control.value) <= max, `The entered number must be less than ${max}`);
    } else {
      this.assert(control.value.length <= max, `Only ${max} characters allowed`);
    }
  }

}
