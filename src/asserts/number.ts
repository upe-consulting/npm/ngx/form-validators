import { FormControl } from '@angular/forms';
import { Assertion } from '../assertion';
import { flag } from '../utils/flag';

export function number(this: Assertion) {
  const control: FormControl = flag(this, 'object');

  flag(this, 'number', true);

  this.assert(!isNaN(Number(control.value || 0)), 'expected #{this} to be null');

}