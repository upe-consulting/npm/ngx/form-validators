import { FormControl } from '@angular/forms';
import { default as moment } from 'moment/src/moment';
import { Assertion } from '../assertion';
import { flag } from '../utils/flag';

export const DATE_REGEX = /^(?:(?:(?:0?[13578]|1[02])(\/|-|\.)31)\1|(?:(?:0?[1,3-9]|1[0-2])(\/|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:0?2(\/|-|\.)29\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:(?:0?[1-9])|(?:1[0-2]))(\/|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;

export let DEFAULT_DATE_FORMAT = 'YYYY-MM-DD';

export let LANG = 'de';

export function setDefaultDateLang(lang) {
  LANG = lang;
}

export function SetDefaultDateFormat(format) {
  DEFAULT_DATE_FORMAT = format;
}

export function isDate(dateString: string): boolean {
  return moment(dateString, DEFAULT_DATE_FORMAT, LANG, true).isValid();
}

export function toDate(dateString: string): Date | null {
  const momentDate = moment(dateString, DEFAULT_DATE_FORMAT, LANG, true);
  return momentDate.isValid() ? momentDate.toDate() : null;
}

export function assertDate(this: Assertion) {
  const control: FormControl = flag(this, 'object') || this._obj;

  flag(this, 'date', true);

  this.assert(!control.value || isDate(control.value), 'expected #{this} to be null');

}