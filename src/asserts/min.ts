import { FormControl } from '@angular/forms';
import { Assertion } from '../assertion';
import { flag } from '../utils/flag';

export function assertMin(this: Assertion, min: number) {
  const control: FormControl = flag(this, 'object');

  if (control.value) {
    if (flag(this, 'number')) {
      this.assert(Number(control.value) >= min, `The entered number must be gather than ${min}`);
    } else {
      this.assert(control.value.length >= min, `Minimum ${min} characters required`);
    }
  }

}
