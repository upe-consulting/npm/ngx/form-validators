export class ValidationError {

  public constructor(public msg: string) {
  }

}
