import { flag } from './utils/flag';

export function assertLengthChain() {
  flag(this, 'doLength', true);
}