// Karma configuration for Unit testing

const path = require('path');

module.exports = function (config) {

  const configuration = {

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',

    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine', 'mocha', 'chai', 'sinon-chai'],

    plugins: [
      require('karma-mocha'),
      require('karma-chai-plugins'),
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-firefox-launcher'),
      require('karma-webpack'),
      require('karma-sourcemap-loader'),
      require('karma-spec-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('istanbul-instrumenter-loader')
    ],

    // list of files / patterns to load in the browser
    files: [
      {pattern: 'spec.bundle.js', watched: false}
    ],

    // list of files to exclude
    exclude: [],

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      'spec.bundle.js': ['webpack', 'sourcemap']
    },

    // webpack
    webpack: {
      resolve:     {
        extensions: ['.ts', '.js']
      },
      module:      {
        rules:               [
          {
            test:    /\.ts/,
            use:     [
              {loader: 'ts-loader'},
              {loader: 'source-map-loader'}
            ],
            exclude: /node_modules/
          },
          {
            enforce: 'post',
            test:    /\.ts/,
            use:     [
              {
                loader:  'istanbul-instrumenter-loader',
                options: {esModules: true}
              }
            ],
            exclude: [
              /\.spec.ts/,
              /node_modules/
            ]
          }
        ],
        exprContextCritical: false
      },
      devtool:     'inline-source-map',
      performance: {hints: false}
    },

    webpackServer: {
      noInfo: true
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['spec', 'coverage-istanbul'],

    coverageIstanbulReporter: {
      reports:               ['html', 'json-summary', 'text-summary'],
      dir:                   path.join(__dirname, 'coverage'),
      fixWebpackSourcePaths: true,
      thresholds:            {
        emitWarning: true, // set to `true` to not fail the test command when thresholds are not met
        global:      { // thresholds for all files
          statements: 80,
          lines:      80,
          branches:   80,
          functions:  100
        }
      }
    },


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome', 'ChromeHeadless', 'Firefox'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true,

    customLaunchers: {
      ChromeHeadlessForRoot: {
        base:  'ChromeHeadless',
        flags: ['--no-sandbox']
      }
    }

  };

  config.set(configuration);

};
