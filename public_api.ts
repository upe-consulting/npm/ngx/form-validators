/**
 * NGX Forms
 * Copyright Merzough Badry Münker
 * MIT license
 * https://gitlab.com/upe-consulting/npm/ngx/form-validators
 */

/**
 * Entry point for all public APIs of the package.
 */
export * from './src/ngx-form-validators';

// This file only reexports content of the `src` folder. Keep it that way.
